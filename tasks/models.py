from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class Actions(models.Model):
    name = models.CharField(max_length=11)
    status = models.CharField(max_length=11)
    price = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True, default=None)
    users_done = models.ManyToManyField(User, blank=True, default=None)


class TasksManager(models.Manager):
    def add_step_to_task(self, **kwargs):
        action = Actions.objects.create(name=kwargs['action_name'], status='todo')
        Tasks.objects.create(day=kwargs['day'], task=kwargs['task'], action=action, status='todo')

    def get_tasks(self, **kwargs):
        return Tasks.objects.filter(name__isnull=False)


class Tasks(models.Model):
    name = models.CharField(max_length=255, unique=True, null=True)
    day = models.IntegerField(blank=True, null=True, default=None)
    task = models.ForeignKey('Tasks', blank=True, null=True, default=None, on_delete=models.CASCADE)
    action = models.ForeignKey(Actions, blank=True, null=True, default=None, on_delete=models.CASCADE)
    status = models.CharField(max_length=25, blank=True, null=True, default=None)
    users_started = models.ManyToManyField(User, blank=True, default=None, related_name='users_started')
    users_done = models.ManyToManyField(User, blank=True, default=None, related_name='users_done')

    @property
    def launches(self):
        return self.tasks_set.filter(action__name='launch').count()

    @property
    def do_review(self):
        return self.tasks_set.filter(action__name='do_review').exists()

    objects = TasksManager()

    def __str__(self):
        return 'task: {}, action: {}'.format(self.task_id, self.action.name)