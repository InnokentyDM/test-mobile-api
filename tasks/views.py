from django.contrib.auth.models import User
from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.generics import ListCreateAPIView, ListAPIView, RetrieveDestroyAPIView, \
    get_object_or_404
from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from tasks.models import Tasks
from tasks.serializers import TaskSerializer, CreateTaskSerializer, TasksListSerializer, UserTasksListSerializer, \
    UserTaskStepsSerializer


class AddTask(ListCreateAPIView):
    queryset = Tasks.objects.get_tasks()
    permission_classes = (IsAdminUser, )

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return CreateTaskSerializer
        if self.request.method == 'GET':
            return TasksListSerializer
        else:
            return TaskSerializer

    # TODO: Improve steps logic
    def create(self, request, *args, **kwargs):
        serializer = CreateTaskSerializer(data=request.data)
        if serializer.is_valid():
            new_task = Tasks.objects.create(name=serializer.validated_data['task_id'], status='todo')
            Tasks.objects.add_step_to_task(day=1, task=new_task, action_name='install')
            for i in range(serializer.validated_data['launches']):
                day = i + 1
                Tasks.objects.add_step_to_task(day=day, task=new_task, action_name='launch')
            if serializer.validated_data['do_review']:
                Tasks.objects.add_step_to_task(day=7, task=new_task, action_name='launch')
                Tasks.objects.add_step_to_task(day=7, task=new_task, action_name='do_review')
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ListTask(ListAPIView):
    queryset = Tasks.objects.get_tasks()
    serializer_class = TasksListSerializer
    permission_classes = (IsAdminUser,)


class DeleteTask(RetrieveDestroyAPIView):
    queryset = Tasks.objects.get_tasks()
    serializer_class = TasksListSerializer
    permission_classes = (IsAdminUser,)

    def get_object(self):
        queryset = self.get_queryset()
        task_id = self.request.query_params.get('task_id', None)
        # TODO: if object doesn't exist return http 400
        obj = get_object_or_404(queryset, name=task_id)
        self.check_object_permissions(self.request, obj)
        return obj


class ListUserTask(ListTask):
    permission_classes = (AllowAny,)
    serializer_class = UserTasksListSerializer

    def get_queryset(self):
        user_id = self.request.query_params.get('user_id', None)
        User.objects.get_or_create(username=user_id)
        return Tasks.objects.get_tasks()


class UserTaskSteps(ListAPIView):
    permission_classes = (AllowAny, )
    serializer_class = UserTaskStepsSerializer

    # TODO: Add user_id support
    # TODO: Group by days
    def get_queryset(self):
        user_id = self.request.query_params.get('user_id', None)
        task_id = self.request.query_params.get('task_id', None)
        instance = Tasks.objects.get(name=task_id)
        users_task_qs = Tasks.objects.filter(task=instance).order_by('day')
        return users_task_qs


class DoTaskAPIView(APIView):
    permission_classes = (AllowAny, )

    def post(self, request, format=None):
        user_id = self.request.query_params.get('user_id', None)
        user = User.objects.get(username=user_id)
        task_id = self.request.query_params.get('task_id', None)
        step_action = self.request.query_params.get('step', None)
        step = Tasks.objects.filter(task__name__in=[task_id, ], action__name=step_action, action__status='todo').first()
        if step:
            step.action.users_done.add(user)
            step.action.status = 'done'
            step.task.status = 'started'
            if not user in step.task.users_started.all():
                step.task.users_started.add(user)
            step.task.save()
            step.action.save()
            step.save()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)