from django.urls import path, include
from tasks.views import AddTask, ListTask, DeleteTask, ListUserTask, UserTaskSteps, DoTaskAPIView


urlpatterns = [
    path('admin/add', AddTask.as_view(), name='add-task'),
    path('admin/list', ListTask.as_view(), name='task-list'),
    path('admin/delete', DeleteTask.as_view(), name='delete-task'),
    path('user/list', ListUserTask.as_view(), name='user-task-list'),
    path('user/task/list', UserTaskSteps.as_view(), name='users-task'),
    path('user/do', DoTaskAPIView.as_view(), name='complete-user-task-step')
]


