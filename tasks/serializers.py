from django.contrib.auth.models import User
from rest_framework import serializers

from tasks.models import Tasks, Actions


class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tasks
        fields = ('name', 'status')
        lookup_field = 'name'


class CreateTaskSerializer(serializers.Serializer):
    task_id = serializers.CharField()
    launches = serializers.IntegerField()
    do_review = serializers.BooleanField()

    def validate(self, data):
        if data['launches'] < 1:
            raise serializers.ValidationError('launches < 1')
        task = Tasks.objects.filter(name=data['task_id']).first()
        if task:
            raise serializers.ValidationError('task already exists')
        return data


class TasksListSerializer(serializers.ModelSerializer):
    launches = serializers.SerializerMethodField()
    do_review = serializers.SerializerMethodField()
    users_started = serializers.SerializerMethodField()
    users_done = serializers.SerializerMethodField()
    task_id = serializers.SerializerMethodField()

    def get_launches(self, instance):
        return instance.launches

    def get_do_review(self, instance):
        return instance.do_review

    def get_users_started(self, instance):
        if instance.users_started.all().exists():
            return instance.users_started.all().values_list('username', flat=True)
        return None

    def get_users_done(self, instance):
        if instance.users_done.all().exists():
            return instance.users_done.all().values_list('username', flat=True)
        return None

    def get_task_id(self, instance):
        return instance.name

    class Meta:
        model = Tasks
        fields = ('task_id', 'launches', 'do_review', 'users_started', 'users_done')


class UserTasksListSerializer(serializers.ModelSerializer):
    task_id = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()

    def get_price(self, instance):
        if instance.action:
            return instance.action.price
        return None

    def get_task_id(self, instance):
        return instance.name

    class Meta:
        model = Tasks
        fields = ('task_id', 'price', 'status')
        lookup_field = 'name'


class ActionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Actions
        fields = ('name', 'status')


class UserTaskStepsSerializer(serializers.ModelSerializer):
    step = ActionsSerializer(source='action')

    class Meta:
        model = Tasks
        fields = ('day', 'step')
